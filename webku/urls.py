from django.urls import path
from . import views

app_name = 'webku'

urlpatterns = [
    path('', views.beranda, name='beranda'),
    path('tentang/', views.tentang, name='tentang'),
    path('kontak/', views.kontak, name='kontak'),
    path('pengalaman/', views.pengalaman, name='pengalaman'),
    path('galeri/', views.galeri, name='galeri'),
]
