from django.shortcuts import render

def beranda(request):
    return render(request, 'Beranda.html')
def galeri(request):
    return render(request, 'Galeri.html')
def kontak(request):
    return render(request, 'Kontak.html')
def pengalaman(request):
    return render(request, 'Pengalaman.html')
def tentang(request):
    return render(request, 'Tentang.html')
# Create your views here.
